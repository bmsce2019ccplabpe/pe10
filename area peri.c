#include<stdio.h>
#include<math.h>
float get_radius()
{
  float radius;
  printf("enter radius\n");
  scanf("%f",&radius);
  return radius;
}
  float compute_area(float radius)
{
  return M_PI*radius*radius;
}
  float compute_perimeter(float radius)
{
  return 2*M_PI*radius;
} 
  float output(float radius,float area,float perimeter)
{
  printf("the area and perimeter of the circle with radius=%f is area=%f perimeter=%f\n",radius,area,perimeter);
}
  int main()
{
  float radius,area,perimeter;
  radius = get_radius();
  area = compute_area(radius);
  perimeter = compute_perimeter(radius);
  output(radius,area,perimeter);
  return 0;
}

